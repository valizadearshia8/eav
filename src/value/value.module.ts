import { Module } from '@nestjs/common';
import { ValueService } from './value.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Attribute } from 'src/attribute/entities/attribute.entity';
import { Value } from './entities/value.entity';
import { AttributeService } from 'src/attribute/attribute.service';
import { Option } from 'src/attribute/entities/option.entity';
import { ValueController } from './value.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Attribute, Value, Option])],
  controllers: [ValueController],
  providers: [ValueService,AttributeService],
})
export class ValueModule {}
