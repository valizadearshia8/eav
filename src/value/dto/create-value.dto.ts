import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { AttributeTypes } from 'src/attribute/entities/attribute.entity';

export class CreateValueDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  entityId: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  attributeId: number;
  @ApiProperty()
  @IsNotEmpty()
  value: any;
}
