import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { CreateValueDto } from './dto/create-value.dto';
import { UpdateValueDto } from './dto/update-value.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Value } from './entities/value.entity';
import {
  Attribute,
  AttributeTypes,
} from 'src/attribute/entities/attribute.entity';
import { AttributeService } from 'src/attribute/attribute.service';
import { isBoolean, isNumber, isString } from 'class-validator';

@Injectable()
export class ValueService {
  @InjectRepository(Value) private readonly valRepo: Repository<Value>;
  @Inject(forwardRef(() => AttributeService))
  private readonly attrService: AttributeService;

  async set(dto: CreateValueDto) {
    const attribute = await this.attrService.findOne(dto.attributeId);
    switch (attribute.type) {
      case AttributeTypes.str:
        this.stringSetter(dto.value, attribute, dto.entityId);
        break;
      case AttributeTypes.int:
        this.numberSetter(dto.value, attribute, dto.entityId);
        break;
      case AttributeTypes.bool:
        this.boolSetter(dto.value, attribute, dto.entityId);
        break;
      default:
        break;
    }
  }

  async getValues(entityCategoryId: string, entityIds: string[]) {
    const schemas = await this.attrService.findAll(entityCategoryId);

    const values = await this.valRepo.find({
      where: {
        entityId: In(entityIds),
        attribute: schemas,
      },
      relations: ['attribute'],
    });
    const attributes = {};
    for (const value of values) {
      let actValue = undefined;
      switch (value.attribute.type) {
        case AttributeTypes.str:
          actValue = value.string;
          break;

        case AttributeTypes.int:
          actValue = value.number;
          break;

        default:
          break;
      }
      if (!attributes[value.entityId]) attributes[value.entityId] = {};
      attributes[value.entityId][value.attribute.label] = actValue;
    }

    return {
      schemas,
      attributes,
    };
  }

  private findAttributeValue(entityId: string, attribute: Attribute) {
    return this.valRepo.findOne({ where: { attribute, entityId } });
  }

  private async stringSetter(
    val: string,
    attribute: Attribute,
    entityId: string,
  ) {
    if (isString(val)) throw new BadRequestException('should be string');

    const exVal = await this.findAttributeValue(entityId, attribute);
    const newVal = exVal
      ? (() => {
          exVal.string = val;
          return exVal;
        })()
      : this.valRepo.create({
          attribute,
          entityId,
          string: val,
        });
    return this.valRepo.save(newVal);
  }

  private async numberSetter(
    val: number,
    attribute: Attribute,
    entityId: string,
  ) {
    if (isNumber(val)) throw new BadRequestException('should be string');
    const exVal = await this.findAttributeValue(entityId, attribute);
    const newVal = exVal
      ? (() => {
          exVal.number = val;
          return exVal;
        })()
      : this.valRepo.create({
          attribute,
          entityId,
          number: val,
        });
    return this.valRepo.save(newVal);
  }

  private async boolSetter(
    val: boolean,
    attribute: Attribute,
    entityId: string,
  ) {
    if (isBoolean(val)) throw new BadRequestException('should be string');
    const exVal = await this.findAttributeValue(entityId, attribute);
    const newVal = exVal
      ? (() => {
          exVal.boolean = val;
          return exVal;
        })()
      : this.valRepo.create({
          attribute,
          entityId,
          boolean: val,
        });
    return this.valRepo.save(newVal);
  }

  
}
