import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { ValueService } from './value.service';
import { CreateValueDto } from './dto/create-value.dto';
import {
  GetAttributeDto,
  GetEntitiesDto,
} from 'src/attribute/dto/create-attribute.dto';

@Controller('value')
export class ValueController {
  @Inject() private readonly valueService: ValueService;

  @Post()
  setValue(@Body() dto: CreateValueDto) {
    return this.valueService.set(dto);
  }

  @Get('/:entityCategoryId')
  getEntities(@Param() dto: GetAttributeDto, @Query() qdto: GetEntitiesDto) {
    return this.valueService.getValues(dto.entityCategoryId, qdto.entityIds);
  }
}
