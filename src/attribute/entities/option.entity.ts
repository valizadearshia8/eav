import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Attribute } from './attribute.entity';

@Entity()
export class Option {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  option: string;

  @ManyToOne((type) => Attribute, (attribute) => attribute.options)
  attribute: Attribute;
}
