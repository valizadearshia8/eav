import { Attribute } from 'src/attribute/entities/attribute.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Value {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  entityId: string;

  @Column({
    nullable: true,
  })
  string: string;

  @Column({
    nullable: true,
  })
  number: number;

  @Column({
    nullable: true,
  })
  boolean: boolean;

  @Column({
    nullable: true,
    type: 'timestamptz',
  })
  date: Date;

  @ManyToOne((type) => Attribute, (Attribute) => Attribute.values)
  attribute: Attribute;
}
