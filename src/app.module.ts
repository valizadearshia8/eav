import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import conf from './utils/config';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { TerminusModule } from '@nestjs/terminus';
import { AttributeModule } from './attribute/attribute.module';
import { ValueModule } from './value/value.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.development.local', '.env.development', '.env'],
      load: [conf],
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: conf().database.host,
      port: conf().database.port,
      username: conf().database.username,
      password: conf().database.password,
      database: conf().database.name,
      autoLoadEntities: true,
      synchronize: conf().database.synchronize
        ? conf().database.synchronize === 'true'
        : true,
    }),
    TerminusModule.forRoot(),
    ScheduleModule.forRoot(),
    AttributeModule,
    ValueModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
