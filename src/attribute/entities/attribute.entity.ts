import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Option } from './option.entity';
import { Value } from 'src/value/entities/value.entity';

export enum AttributeTypes {
  str = 'string',
  int = 'integer',
  bool = 'boolean',
  opt = 'option',
}

@Entity()
export class Attribute {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    enum: AttributeTypes,
  })
  type: AttributeTypes;

  @Column()
  label: string;

  @Column()
  entityCategoryId: string;

  @OneToMany((type) => Value, (Value) => Value.attribute, { cascade: true, })
  values: Value;

  @OneToMany((type) => Option, (Option) => Option.attribute, { cascade: true })
  options: Option[];
}
