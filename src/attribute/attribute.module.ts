import { Module } from '@nestjs/common';
import { AttributeService } from './attribute.service';
import { AttributeController } from './attribute.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Attribute } from './entities/attribute.entity';
import { Value } from 'src/value/entities/value.entity';
import { Option } from './entities/option.entity';
import { ValueService } from 'src/value/value.service';

@Module({
  imports: [TypeOrmModule.forFeature([Attribute, Value, Option])],
  controllers: [AttributeController],
  providers: [AttributeService , ValueService],
})
export class AttributeModule {}
