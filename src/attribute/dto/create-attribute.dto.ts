import { ApiProperty } from '@nestjs/swagger';
import { AttributeTypes } from '../entities/attribute.entity';
import { IsArray, IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class GetAttributeDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  entityCategoryId: string;
}


export class GetEntitiesDto{
  @ApiProperty({
    isArray:true
  })
  @IsArray({

  })
  entityIds:string[]
}


export class CreateAttributeDto extends GetAttributeDto {
  @ApiProperty({ enum: AttributeTypes })
  @IsEnum(AttributeTypes)
  type: AttributeTypes;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  label: string;
}

export class CreateOptionsDto {
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  attributeId: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  option: string;
}


export class GetOptionDto{
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  optionId: number;
}