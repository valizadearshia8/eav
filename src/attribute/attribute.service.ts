import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import {
  CreateAttributeDto,
  CreateOptionsDto,
} from './dto/create-attribute.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Attribute, AttributeTypes } from './entities/attribute.entity';
import { Option } from './entities/option.entity';
import { ValueService } from 'src/value/value.service';

@Injectable()
export class AttributeService {
  @InjectRepository(Attribute) private readonly attrRepo: Repository<Attribute>;
  @InjectRepository(Option) private readonly optRepo: Repository<Option>;
  @Inject() private readonly valService: ValueService;

  create(createAttributeDto: CreateAttributeDto) {
    return this.attrRepo.save(
      this.attrRepo.create({
        ...createAttributeDto,
      }),
    );
  }

  async addOption(dto: CreateOptionsDto) {
    const attribute = await this.findOne(dto.attributeId);
    if (attribute.type !== AttributeTypes.opt)
      throw new BadRequestException('attribute.not_optional');
    return this.optRepo.save(
      this.optRepo.create({
        attribute,
        option: dto.option,
      }),
    );
  }

  async removeOption(id: number) {
    const option = await this.findOption(id);
    return this.optRepo.remove([option]);
  }

  async findOption(id: number, attribute?: Attribute) {
    const option = await this.optRepo.findOne({
      where: {
        id,
        attribute,
      },
    });
    if (!option) throw new NotFoundException('option');
    return option;
  }

  async findOne(id: number) {
    const attr = await this.attrRepo.findOne({
      where: {
        id,
      },
    });
    if (!attr) throw new NotFoundException('attribute');
    return attr;
  }

  findAll(entityCategoryId: string, options = true) {
    return this.attrRepo.find({
      where: { entityCategoryId },
      relations: {
        options,
      },
    });
  }

  async remove(id: number) {
    const attribute = await this.findOne(id);
    return this.attrRepo.remove([attribute]);
  }

  getEntityAttribute(entityCategoryId: string, entityIds: string[]) {
    this.attrRepo
      .createQueryBuilder('attribute')
      .where('') /// select category
      // .innerJoin()
      .whereInIds(entityIds) /// select entiry id
      .groupBy('entityId')
      .execute();
  }
}
