import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { AttributeService } from './attribute.service';
import {
  CreateAttributeDto,
  CreateOptionsDto,
  GetAttributeDto,
  GetOptionDto,
} from './dto/create-attribute.dto';

@Controller('attribute')
export class AttributeController {
  constructor(private readonly attributeService: AttributeService) {}

  @Post()
  create(@Body() createAttributeDto: CreateAttributeDto) {
    return this.attributeService.create(createAttributeDto);
  }

  @Post('/option')
  addOption(@Body() dto: CreateOptionsDto) {
    return this.attributeService.addOption(dto);
  }

  @Delete('/option/:optionId')
  removeOption(@Param() dto: GetOptionDto) {
    return this.attributeService.removeOption(dto.optionId);
  }

  @Get('/:entityCategoryId')
  getCategoryAttributes(@Param() dto: GetAttributeDto) {
    return this.attributeService.findAll(dto.entityCategoryId);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.attributeService.remove(+id);
  }
}
